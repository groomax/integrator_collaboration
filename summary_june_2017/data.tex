\subsection*{\texorpdfstring{\cpp{StateManager}}{StateManager}}

All data used in \cpp{do_md} in the current code base was collected in
the \cpp{StateManager} class by making the variables public member
variables of the \cpp{StateManager} class. A \cpp{StateManager} object
is accessible to all elements, rendering all variables readable and
writable by any element. While this new approach mimics (and exposes) the
design currently used, it is neither really programmer-friendly nor
very safe.

The envisioned data reorganization has several goals. First, it should
increase general code readability, by renaming variables using
expressive variable names, thorough commenting, and possibly grouping
of variables in logical sub-units. Second, it should encapsulate data
wherever possible to reduce the likelihood of bugs originating in
elements unexpectedly writing to the same variables. Third, it should
facilitate the saving of states for different purposes (restoring of
simulation states in MC-like simulations, saving energies for MC
acceptance criteria, caching of states for trajectory writing,
etc.). Fourth, it should allow for facilitated validation of states,
to simplify testing of custom integrators. And lastly, the loss of
performance due to any overhead introduced must be kept negligible.

Grouping of variables in logical sub-units (\emph{goal 1}) could occur in different
ways. One possibility is grouping by writing access:
\begin{itemize}
\item Constant at integrator level: Values passed into the integrator
  which are never changed within.
\item Constant at loop level: Values which are only changed during
  loop setup or loop teardown.
\item Constant at element level: Values which are changed during step
  setup and step teardown, but not by elements.
\item Variable by specific elements: Values changed in very specific
  elements (e.g. force calculation, global communication, state
  writing / resetting) only.
\item Element variables: Variables which can be changed by general
  elements. These should typically only include the physical state
  variables such as positions, velocities, energies, etc.
\end{itemize}
Another possible grouping scheme is by logical meaning of the
variables:
\begin{itemize}
\item Program environment: Platform information, communication /
  domain decomposition, IO / logging, ...
\item System information: Topological data like force field
  parameters, atom types, ...
\item Physical state: Positions, velocities, forces, energies,
  coupling variables, ...
\end{itemize}
These approaches can of course be combined.

The facilitated saving of states (\emph{goal 3}) might at times be in
conflict with a rigorous encapsulation (\emph{goal 2}). An example of
this is data specific to a unique element such as the force
calculation, which, from a point of view of proper encapsulation,
should become a member variable of said element. To facilitate the
restoring of integration states, however, it might be more convenient
to save such variables in a central location like the
\cpp{StateManager}.

``Saving the state'' (\emph{goal 3}) can be understood in different ways:
\begin{enumerate}
\item Save as checkpoint: Already present currently, checkpointing is
  saving the state of the system. It requires a call to the force
  calculation and the globals computing elements to restore a
  functional integrator state.
\item Save for MC comparisons: To be able to compare two state in a
  MC-acceptance scenario, only a subset of each state might be
  needed. It is very important, however, to ensure that the comparison
  criteria are chosen at the correct time step, which can be
  non-trivial depending on the integration algorithm. As an example,
  using the leap-frog integrator, the kinetic energy might be a time
  step behind the potential energy at the completion of an integrator
  step, a potential problem that needs to be accounted for when saving
  the state.
\item Save to restore integrator state: Unlike for the MC comparison,
  the state saved to actually restore a previous integrator might need to be
  out-of-sync, if the specific integrator algorithm requires it. In
  case of the leap-frog integrator, the kinetic and the potential
  energy being out-of-sync might actually be necessary to ensure
  proper continuation of the algorithm. Unlike the current
  check-pointing system, restores might be frequent, meaning that
  force calculation and global communication at every restore might
  not be the right choice.
\item Cache for trajectory writing: Like in the case of MC
  comparisons, for trajectory writing it is important to ensure the
  state is in sync.
\end{enumerate}
In summary, states must be saved both in sync and as a snap-shot of an
arbitrary state. States need to contain different information for
different purposes, and backward compatibility with the old
check-pointing system should be kept. Also, saved states should be
robust towards changes in the domain decomposition between them.

To facilitate the validation (\emph{goal 4}), we envision to add
information to the state about how far it has progressed through the
fractional timestep.  Every element propagating the state is naturally
doing so by integrating over a prescribed timestep, which can be tracked. Allowing the
\cpp{StateManager} to keep track of this would massively simplify both
the validation of integrator algorithms and the saving of states at
well defined points in time.


\iffalse
The following code part shows all member
variables of \cpp{StateManager}:

\begin{lstlisting}[style=C++Style, basicstyle=\linespread{0.9}\ttfamily\scriptsize]
class StateManager
{
public:
  // Passed in from do_md
  FILE                     *fplog;
  t_commrec                *cr;
  const gmx::MDLogger      *mdlog;
  int                       nfile;
  const t_filenm           *fnm;
  const gmx_output_env_t   *oenv;
  gmx_bool                  bVerbose;
  int                       nstglobalcomm;
  gmx_vsite_t              *vsite;
  gmx_constr_t              constr;
  int                       stepout;
  t_inputrec               *ir;
  gmx_mtop_t               *top_global;
  t_fcdata                 *fcd;
  t_state                  *state_global;
  energyhistory_t          *energyHistory;
  t_mdatoms                *mdatoms;
  t_nrnb                   *nrnb;
  gmx_wallcycle_t           wcycle;
  gmx_edsam_t               ed;
  t_forcerec               *fr;
  int                       repl_ex_nst;
  int                       repl_ex_nex;
  int                       repl_ex_seed;
  gmx_membed_t             *membed;
  real                      cpt_period;
  real                      max_hours;
  int                       imdport;
  unsigned long             Flags;
  gmx_walltime_accounting_t walltime_accounting;

  // do_md local variables
  gmx_mdoutf_t      outf;
  gmx_int64_t       step, step_rel;
  double            elapsed_time;
  double            t, t0, lam0[efptNR];
  gmx_bool          bGStatEveryStep, bGStat, bCalcVir, bCalcEnerStep, bCalcEner;
  gmx_bool          bNS, bNStList, bSimAnn, bStopCM,
                    bFirstStep, startingFromCheckpoint, bInitStep, bLastStep,
                    bBornRadii, bUsingEnsembleRestraints;
  gmx_bool          bDoDHDL, bDoFEP, bDoExpanded;
  gmx_bool          do_ene, do_log, do_verbose, bCPT;
  gmx_bool          bMasterState;
  int               force_flags, cglo_flags;
  tensor            force_vir, shake_vir, total_vir, tmp_vir, pres;
  int               i, m;
  rvec              mu_tot;
  t_vcm            *vcm;
  matrix            parrinellorahmanMu, M;
  gmx_repl_ex_t     repl_ex;
  int               nchkpt;
  gmx_localtop_t   *top;
  t_mdebin         *mdebin;
  gmx_enerdata_t   *enerd;
  PaddedRVecVector  f {};
  gmx_global_stat_t gstat;
  gmx_update_t     *upd;
  t_graph          *graph;
  gmx_groups_t     *groups;
  gmx_ekindata_t   *ekind;
  gmx_shellfc_t    *shellfc;
  gmx_bool          bSumEkinhOld, bDoReplEx, bExchanged, bNeedRepartition;
  gmx_bool          bResetCountersHalfMaxH;
  gmx_bool          bTemp, bPres, bTrotter;
  real              dvdl_constr;
  matrix            lastbox;
  int               lamnew;
  
  /* for FEP */
  int               nstfep;
  double            cycles;
  real              saved_conserved_quantity;
  real              last_ekin;
  t_extmass         MassQ;
  int             **trotter_seq;
  char              sbuf[STEPSTRSIZE], sbuf2[STEPSTRSIZE];
  int               handled_stop_condition = gmx_stop_cond_none; /* compare to get_stop_condition*/
  std::unique_ptr<t_state> stateInstance;
  t_state *                state;

  /* PME load balancing data for GPU kernels */
  pme_load_balancing_t *pme_loadbal;
  gmx_bool              bPMETune;
  gmx_bool              bPMETunePrinting;

  /* Interactive MD */
  gmx_bool          bIMDstep;

#ifdef GMX_FAHCORE
  /* Temporary addition for FAHCORE checkpointing */
  int chkpt_ret;
#endif
  /* Domain decomposition could incorrectly miss a bonded
     interaction, but checking for that requires a global
     communication stage, which does not otherwise happen in DD
     code. So we do that alongside the first global energy reduction
     after a new DD is made. These variables handle whether the
     check happens, and the result it returns. */
  bool              shouldCheckNumberOfBondedInteractions;
  int               totalNumberOfBondedInteractions;

  gmx::SimulationSignals signals;
  // Most global communnication stages don't propagate mdrun
  // signals, and will use this object to achieve that.
  gmx::SimulationSignaller nullSignaller;

  /* [Constructors and member functions omitted] */
}
    
\end{lstlisting}

\subsection*{Data categories}

\cPTM{I would suggest to divide the data in three main categories to
  get an overview (see below for the categories). It might be helpful
  to sort the \cpp{StateManager} member variables to reflect these
  categories, additionally to (or instead of) sorting them into
  ``Passed into do\_md'' and ``Local variables of do\_md''. In a next
  step, we can then break it down to subcategories, and get a feeling
  for how we could sensibly restructure them. Also, a comment
  explaining each field would be very useful (as, for example, in the
  \cpp{t_state} snippet below).  Can, let me know if this is something
  that you can do in reasonable time... Maybe you can sort \& comment
  the ones you know off the top of your head, and I'll do the rest? If
  you do changes, it probably makes sense to do the changes directly
  in the cpp source file, and copy them back here after.}

The data used in \cpp{do_md} currently can be divided in the following
categories:

\subsubsection*{State data}

\emph{State data} represents the state of the system. It does
potentially (depending on the integration algorithm, the boundary
conditions, etc.) change during the course of the simulation.

\emph{Note:} New algorithms might need additional parameters
describing the state. Examples: Mario talked about changing the
simulation time step, one could imagine changing the target
temperature or pressure during a simulation, etc...

\emph{Note:} Certain algorithms require some history - would we define
the history part of the state data as well?

\subsubsection*{Variable simulation parameters}
Variable simulation parameters change along the simulations, but do
not define the physical state of the system. Examples include the
current step or the domain decomposition. \cPTM{Feel free to add...}

\subsubsection*{Constant simulation parameters}
Constant simulation parameters do not change along a simulation run,
and may include things like the total number of steps, the time step,
the write-out frequency, etc. 

\subsection*{Stateful data}

On a longer term, I'd suggest to include the current time of the state
in the state data. Our propagator elements naturally progress part of
the state by $\Delta t$ or a fraction of it. Keeping track of this
allows us to check at runtime for errors. I'll give an example: A NVE
Trotter decomposition can look like
\begin{equation}
  v(\Delta t/2) r(\Delta t) v(\Delta t/2) \, ,
\end{equation}
which translates to the velocity-verlet algorithm
\begin{equation}
  \begin{split}
    v(t + \Delta t/2) &= v(t) + \frac{\Delta t}{2} \frac{f(t)}{m} \\
    r(t + \Delta t) &= r(t) + \Delta t \cdot v(t + \Delta t/2) \\
    v(t + \Delta t) &= v(t + \Delta t/2) + \frac{\Delta t}{2}
    \frac{f(t + \Delta t)}{m} \, .
  \end{split}
\end{equation}
Currently, we would write this as a sequence of elements looking
something like this:
\begin{lstlisting}
  update_velocities(dt/2)
  update_positions(dt)
  update_forces()
  update_velocities(dt/2)
\end{lstlisting}
\code{update_velocities}, for example, then takes whatever is
currently saved in forces, and add it to the current velocities.
\begin{lstlisting}
  update_velocities(dt)
  {
    for all atoms n:
      v[n] = v[n] + dt*f[n]/m[n]
  }
\end{lstlisting}
I would suggest to add another optional argument to the elements,
defining the relative position of the update compared to the
state. The algorithm would then looks something like this:
\begin{lstlisting}
  update_velocities(dt/2, 0)
  update_positions(dt, dt/2)
  update_forces()
  update_velocities(dt/2, dt/2) 
\end{lstlisting}
and the \code{update_velocities} could make sure it's using the right
update:
\begin{lstlisting}
  update_velocities(dt, rel_dt)
  {
    t = current_time(v)
    for all atoms n:
      v[n][t+dt] = v[n][t] + dt*f[n][t+rel_dt]/m[n]
  }
\end{lstlisting}
Possible errors can then be handled at the state manager level - the
element is only asking for specific data.

\subsection*{Various data copied here from wiki / previous documents
  in case they become relevant}

\cPTM{The following is copied from the wiki, discussing which data
  should be cached. Copied here in case it becomes useful.}

What are the data to be cached, what properties should they have?

\begin{itemize}
\item The data to be cached should be data that potentially changes
  (depending on the algorithm) with the integrator timestep. Thus the
  cached data should fully represent the data of an integrator at a
  specific timestep.
\item Using this cached data, we should be able to reset the
  integrator to a specific timestep, and continue the integratiton
  from that timestep
\end{itemize}

Gromacs data structures:

\begin{itemize}
\item \cpp{t_state} struct, defined in \code{mdtypes/state.h}
\begin{lstlisting}[style=C++Style, basicstyle=\linespread{0.9}\ttfamily\scriptsize]
typedef struct t_state
{
  int                 natoms;
  int                 ngtc;
  int                 nnhpres;
  int                 nhchainlength;  /* number of nose-hoover chains */
  int                 flags;          /* Flags telling which entries are present */
  int                 fep_state;      /* indicates which of the alchemical states we are in */
  std::vector<real>   lambda;         /* lambda vector */
  matrix              box;            /* box vector coordinates */
  matrix              box_rel;        /* Relitaive box vectors to preserve shape */
  matrix              boxv;           /* box velocitites for Parrinello-Rahman pcoupl */
  matrix              pres_prev;      /* Pressure of the previous step for pcoupl */
  matrix              svir_prev;      /* Shake virial for previous step for pcoupl */
  matrix              fvir_prev;      /* Force virial of the previous step for pcoupl */
  std::vector<double> nosehoover_xi;  /* for Nose-Hoover tcoupl (ngtc) */
  std::vector<double> nosehoover_vxi; /* for N-H tcoupl (ngtc) */
  std::vector<double> nhpres_xi;      /* for Nose-Hoover pcoupl for barostat */
  std::vector<double> nhpres_vxi;     /* for Nose-Hoover pcoupl for barostat */
  std::vector<double> therm_integral; /* for N-H/V-rescale tcoupl (ngtc) */
  real                veta;           /* trotter based isotropic P-coupling */
  real                vol0;           /* initial volume,required for computing NPT conserverd quantity */
  PaddedRVecVector    x;              /* the coordinates (natoms) */
  PaddedRVecVector    v;              /* the velocities (natoms) */
  PaddedRVecVector    cg_p;           /* p vector for conjugate gradient minimization */

  ekinstate_t         ekinstate;       /* The state of the kinetic energy data */

  /* History for special algorithms, should be moved to a history struct */
  history_t           hist;            /* Time history for restraints */
  swapstate_t        *swapstate;       /* Position swapping */
  df_history_t       *dfhist;          /* Free energy history for free energy analysis */
  edsamstate_t       *edsamstate;      /* Essential dynamics / flooding history */

  int                 ddp_count;       /* The DD partitioning count for this state */
  int                 ddp_count_cg_gl; /* The DD part. count for index_gl */
  std::vector<int>    cg_gl;           /* The global cg number of the local cgs */
} t_state;
\end{lstlisting}

\item \cpp{enerd} struct, defined in \code{mdtypes/forcerec.h}
\begin{lstlisting}[style=C++Style, basicstyle=\linespread{0.9}\ttfamily\scriptsize]
typedef struct gmx_enerdata_t {
  real              term[F_NRE];         /* The energies for all different interaction types */
  gmx_grppairener_t grpp;
  double            dvdl_lin[efptNR];    /* Contributions to dvdl with linear lam-dependence */
  double            dvdl_nonlin[efptNR]; /* Idem, but non-linear dependence */
  int               n_lambda;
  int               fep_state;           /* current fep state -- just for printing */
  double           *enerpart_lambda;     /* Partial energy for lambda and flambda[] */
  real              foreign_term[F_NRE]; /* alternate array for storing foreign lambda energies */
  gmx_grppairener_t foreign_grpp;        /* alternate array for storing foreign lambda energies */
} gmx_enerdata_t;
\end{lstlisting}

\item \cpp{ekind} struct, defined in \code{mdtypes/group.h}
\begin{lstlisting}[style=C++Style, basicstyle=\linespread{0.9}\ttfamily\scriptsize]
typedef struct gmx_ekindata_t {
  gmx_bool       bNEMD;
  int            ngtc;            /* The number of T-coupling groups */
  t_grp_tcstat  *tcstat;          /* T-coupling data */
  tensor       **ekin_work_alloc; /* Allocated locations for *_work members */
  tensor       **ekin_work;       /* Work arrays for tcstat per thread */
  real         **dekindl_work;    /* Work location for dekindl per thread */
  int            ngacc;           /* The number of acceleration groups */
  t_grp_acc     *grpstat;         /* Acceleration data */
  tensor         ekin;            /* overall kinetic energy */
  tensor         ekinh;           /* overall 1/2 step kinetic energy */
  real           dekindl;         /* dEkin/dlambda at half step */
  real           dekindl_old;     /* dEkin/dlambda at old half step */
  t_cos_acc      cosacc;          /* Cosine acceleration data */
} gmx_ekindata_t;
\end{lstlisting}

\item Data in \cpp{do_md scope} not being part of a struct
\begin{lstlisting}[style=C++Style, basicstyle=\linespread{0.9}\ttfamily\scriptsize]
tensor  force_vir;
tensor  shake_vir;
tensor  total_vir;
tensor  pres;
\end{lstlisting}

\end{itemize}


\cPTM{The following was copied from the previous \code{integrator.pdf}
  document, thought as a start to improve on. Probably not necessary
  anymore.}

\emph{Data Flow}. There are three types of data that need to flow into
the elements.

\begin{enumerate}
\item The first is data that remains constant for the entirety of the
  simulation, such as $\Delta t$ for propagators, and for inspectors,
  things like the number of DOF, the reference temperatures, the
  properties of the atoms, and so forth. These will be copied into the
  elements at the initialization of the element objects on integrator
  construction at the beginning of the simulation -- or for data this
  shared, they will be placed in the \texttt{Simulation} class, which
  the elements will be able to access. \cMRS{not sure I know what you
    mean by ``data that is shared'' in this context.}
\item The second class are things that are set in the loop setup,
  \cMRS{not sure ``loop setup'' is defined well previous to this} but
  do not change during evaluation of the elements of the
  integrator. These will be updated using the \texttt{update} method
  of the \texttt{Element} class.  This can include things like
  \texttt{start} and \texttt{nrend}, which can change from loop to
  loop depending on load balancing.
\item Finally, things that change during the evaluation of the list of
  elements of the integrator. By definition, these are either part of
  the dynamic state, which will be store in the \texttt{Simulation}
  class, or output of the inspector, like the kinetic energy,
  pressure, and energy, which will also be stored in the
  \texttt{Simulation} class.
\end{enumerate}


List of the elements and the data required for each element (being
updated)

\begin{itemize}
\item momentum step
  \begin{itemize}
  \item Requires: f, a scalar function of box vector variables (for
    NPT), and a scalar function of heat bath variables, TC variables,
    Acceleration variables.
  \item Alters: v
  \end{itemize}

\item position constraint (requires halo communication)
  \begin{itemize}
  \item Requires: x, masses, atom numbers, equilibrium lengths, box
    vector, dt, information variables (constr structure)
  \item Alters: x, virial, dvdlambda, Langevin multipliers
  \end{itemize}

\item velocity constraint (requires halo communication)
  \begin{itemize}
  \item Requires: x, v, dt, atom numbers, masses, equilibrium lengths,
    Langevin multipliers (from the position constraint step)
  \item Alters: v, virial, dvdlambda (mass)
  \end{itemize}

\item Stochastic thermalization (Orstein-Uehlenbeck
  operator)
  \begin{itemize}
  \item Requires: random variable state, T, friction, dt, masses
  \item Alters: p or box vector momentum or lambda momentum all
    (equivalent math), random variable state.
  \end{itemize}

\item Brownian dynamics (may be somewhat separate integrator -
  investigate)
  \begin{itemize}
  \item Requires: T, x, mass, forces, random variable state
  \item Alters: x, random variable state. Does not affect velocity
  \end{itemize}

\item Heat bath velocity increment
  \begin{itemize}
  \item Requires: KE, T
  \item Alters: xi
  \end{itemize}

\item Velocity scaling by some variable
  \begin{itemize}
  \item Requires: T, P, b (any other variables that affect the box
    vector scaling?).
  \item Alters: v
  \end{itemize}

\item Box vector scaling
  \begin{itemize}
  \item Requires:
  \item Alters: $b$
  \end{itemize}

\item Box vector velocity scaling
  \begin{itemize}
  \item Requires:
  \item Alters: $\dot{b}$
  \end{itemize}

\item MC coordinate moves:
  \begin{itemize}
  \item Requires: U
  \item Alters: x
  \end{itemize}

\item MC state moves:
  \begin{itemize}
  \item Requires: U
  \item Alters: x, lambda
  \end{itemize}


\item MC box vector move:
  \begin{itemize}
  \item Requires: U
  \item Alters: b, x (possibly v's)
  \end{itemize}
\end{itemize}
\fi
