The \cpp{loop_setup()}, \cpp{step_setup()}, \cpp{step_teardown()}, and
\cpp{loop_teardown()} functions are planned to be operating on
integrator-independent data \emph{only}. These variable are
independent of the actual integrator algorithm, but indispensable for
the simulation run to function properly and need proper
initialization, updating during the run and finalization. Typical use
of such variables include parallelization, counters, logging, etc.

Below is a detailed explanation what each of the function currently
does, including some suggestions on where some of this functionality
should eventually be moved to.

\subsection*{\texorpdfstring{\cpp{loop_setup}}{loop\_setup}}
\cpp{loop_setup} is only called once before the start of the
integrator loop. The functionality of \cpp{loop_setup} is detailed in
the following list.
\begin{itemize}
\item Reset \cpp{wcycle} counter.
\item Set the \cpp{bTrotter} boolean. \emph{This is integrator-dependent and
  should be moved elsewhere.}
\item Set the stride for global communication calls,
  \cpp{nstglobalcomm}. \emph{This might not be needed, since it will be
  decided by the element. But dependencies should be checked before
  removing.}
\item Set the boolean for \cpp{bGStatEveryStep}, which depends on the
  value of \cpp{nstglobalcomm}.
\item Initialize ion swapping.
\item Initialize md variables via \cpp{init_md}.
\item Initialize energy terms, \cpp{enerd} and \cpp{ekind}.
\item Initialize \cpp{gstat}.
\item Initialize \cpp{shellfc} by \cpp{init_shell_flexcon}. This
  checks for polarizable models and flexible constraints.
\item For non-equilibrium MD, set reference box if deform mdp option
  is given. \emph{This is integrator dependent, thus should be considered to
  be moved into a corresponding element.}
\item If domain decomposition is active, create a local \cpp{state}
  and \cpp{top} from the respective global variables. Otherwise copy
  the global into the local ones.
\item Set up interactive MD.
\item If domain decomposition, distribute the charge groups over the
  nodes from the master node.
\item Initialize expanded ensemble. \emph{This is integrator dependent
    thus should be moved to a corresponding element.}
\item Set constraints, \cpp{set_constraints}. \emph{This might also be moved
  into the constrain element.}
\item Initialize replica exchange. \emph{Should be moved to a
    corresponding element.}
\item Initialize pme load balance.
\item If continuation, clear the velocities, only set the vsites,
  shells and frozen atom's velocities to zero.
\item Construct virtual sites.
\item Set free energy calculation frequency as the greatest common
  denominator of \cpp{nstdhdl} and \cpp{repl_ex_nst}.
\item Call \cpp{compute_globals}. \emph{This call might be more suitable to
  be put into the \cpp{preRunSequence}.}
\item Initialize the Trotter sequence.
\item Start walltime accounting.
\item Set \cpp{bSumEkinhOld}, \cpp{bExchanged}, \cpp{bNeedRepartition}
  to \cpp{FALSE}. \cpp{bFirstStep} to \cpp{True}. And set
  \cpp{bInitStep}, \cpp{bUsingEnsembleRestraints}. \emph{When implementing
  the corresponding elements we should be able to get rid of these
  booleans.}
\item Set the \cpp{signals} to synchronize all simulations. \emph{Is
    used for replica Exchange and ensembleRetraints. Thus might be
    possible to move this into an element related to
    multi-simulations.}
\item Set the \cpp{step} and \cpp{step_rel}.
\end{itemize}

\subsection*{\texorpdfstring{\cpp{step_setup}}{step\_setup}}
\cpp{step_setup} is called at the beginning of every integrator
step. The functionality of \cpp{step_setup} is detailed in the
following list.
\begin{itemize}
\item Set \cpp{bNStList}, to determine if this is a neighbor search
  step. \emph{This could be moved to a neighbor list search element,
    once such an element is created.}
\item Do pme load balance.
\item Start wallcycle accounting.
\item Update the variable \cpp{t} and \cpp{bLastStep}.
\item Find and set the current lambdas. \emph{This should be moved the
    lambda dynamics related elements.}
\item Set \cpp{bDoReplEx}. \emph{This should be moved into a replica
    exchange related element.}
\item Update annealing target temperature. \emph{This should be moved into a
  simulated annealing element.}
\item Update \cpp{bStopCM}. \emph{This could be moved into the computeGlobal
  element, where the CM motion removal is done.}
\item Set boolean \cpp{bNS}, to determine whether or not a neighbor
  search should be done.
\item Determine whether or not to update the Born radii if doing GB
  implicit solvent, by setting \cpp{bBornRadii}. \emph{Should probably
    be moved into an element that is related to GB implicit solvent.}
\item If \cpp{bNS} and is not the last step or a continuation step
  then correct the box vector, and re-partition the system via domain
  decomposition.
\item Update lambda if using lambda dynamics. \emph{Should be moved to
    lambda dynamics related element.}
\item If \cpp{bExchanged}, do compute globals. \emph{This should be part of
  a integrator sequence.}
\end{itemize}

\subsection*{\texorpdfstring{\cpp{step_teardown}}{step\_teardown}}
\cpp{step_teardown} is called at the end of every integrator step. The functionality
of \cpp{step_teardown} is detailed in the following list.
\begin{itemize}
\item If \cpp{doExpanded} set the \cpp{fep_state} to \cpp{lamnew},
  should be moved to lambda dynamics related elements.
\item Print the remaining wallclock time.
\item Do swap coordinates if it is not the last step. \emph{Once a
    swap coordinates element is present this could be moved there.}
\item Do replica exchange. \emph{This should be moved to a replica
    exchange element.}
\item Do domain decomposition.
\item Set \cpp{bFirstStep}, \cpp{bInitStep},
  \cpp{startingFromCheckpoint} to \cpp{FALSE}.
\item Copy the current pressure to \cpp{state->pres_prev}.
\item Increase the \cpp{step} and \cpp{step_rel} by one.
\item Reset all counters.
\item If \cpp{bIMD} is \cpp{True}: The master updates the IMD energy
  record and sends positions to VMD client.
\end{itemize}

\subsection*{\texorpdfstring{\cpp{loop_teardown}}{loop\_teardown}}
\cpp{loop_teardown} is only called once after the integrator loop. The
functionality of \cpp{loop_teardown} is detailed in the following
list.
\begin{itemize}
\item Closing TNG files.
\item End walltime accounting.
\item Tell the PME-only node to finish.
\item Print out final energies to log file.
\item Close the mdout file handle.
\item Finish pme load balancing.
\item Finish \cpp{shellfc}.
\item Print replica exchange statistics.
\item Finish swapping coords (for replica exchange).
\item Finalize interactive MD (IMD).
\end{itemize}

The above breakdown of the functionalities of each function captures
the current status of the code. The goal is to reduce the
functionalities to be integrator independent, and move integrator or
element dependent functionalities into respective elements which can
be called in the preRunSequence.

% LocalWords:  teardown integrator wcycle bTrotter nstglobalcomm md init enerd
% LocalWords:  ekind gstat polarizable dependen pme vsites nstdhdl repl nst rel
% LocalWords:  preRunSequence walltime bSumEkinhOld bExchanged bNeedRepartition
% LocalWords:  bFirstStep bInitStep bUsingEnsembleRestraints ensembleRetraints
% LocalWords:  bNStList wallcycle bLastStep bDoReplEx bStopCM computeGlobal bNS
% LocalWords:  bBornRadii doExpanded fep lamnew wallclock pres prev bIMD IMD
% LocalWords:  startingFromCheckpoint VMD TNG mdout coords
