One of the most important features currently missing is the
restructuring of the data structures. This is discussed in
Section~\ref{sec:data-reorganization}. Other planned extensions are
detailed in the following subsections.

\subsection*{Reorganization of the loop and step setup / teardown}


The \cpp{loop_setup()}, \cpp{step_setup()}, \cpp{step_teardown()}, and
\cpp{loop_teardown()} functions are planned to be operating on
integrator-independent data \emph{only}. These are variables which are
independent of the actual integrator algorithm, but indispensable for
the simulation run to function properly and may need proper
initialization, updating during the run and finalization. Typical use
of such variables include parallelization, counters, logging,
trajectory-writing, etc.  Any data related to the integrator algorithm
should then be handled by elements, either in the initialization of
the elements, or in the pre-run sequence, the main sequence, or the
post-run sequence.

Currently, the separation into integrator-independent and
integrator-dependent functions is not completely
finished. Appendix~\ref{sec:details-loop-step} details the full
functionality of the setup and teardown functions, and notes
functionality which is not yet properly separated. It turns out that
most of the functionality that is yet to be outsourced to elements
concerns elements that have not been written yet, as we are only
slowly building up the functionality available under the new
framework. Examples include updating of variables connected to replica
exchange, lambda dynamics or simulated annealing, which will be
naturally part of respective elements once they are created. We prefer
to currently keep these functionalities in place, as this should make
the creation of elements easier and reduce the chance of introducing
incompatibilities in the planned data reorganization.


\subsection*{Integrator performance}

With the current framework, instead of having one loop over all atoms
as in the current code (or, mostly one loop), there will be multiple
loops over atoms, each of which carries out a simpler procedure, and
there is some overhead to calling multiple loops, and potentially
performance loss due to using different memory access patterns.  If
there are particular code paths we want to have maximum speed, we can
\emph{fuse} elements together, creating a larger element.  This can
essentially be seen as a recreation of the current logic paths in
\cpp{do_update()}, eliminating any duplicated loops or global
communcation calls.  We anticipate that in the majority of cases, such
fused elements would not be needed as the performance cost should be
in the 2-3\% range (based on current amount of time that the
integrators take), with the exception of some issues involving some
types of communication that might occur, most of which can likely be worked
around. These fused elements can then be automatically verified
versus an integrator loop with the elements that they are designed to
be equivalent to.

\subsection*{Custom integrator parsing}

Currently, the integrators implemented in the new framework are
hardcoded--specific mdp options translate to specific sequences
created by the \cpp{SequenceFactory} (see
Section~\ref{sec:current-status}). On a longer term, we want to make
it possible for users to define custom integrators at runtime,
\emph{i.e.} allowing users to arange elements to create new
integrators without needing to recompile the code. We are looking into
ways to achieve that, with a string parser being currently the most
promising idea.

\subsection*{Integrator testing}

In close connection to the previous point, we are also looking into
possibilities to automatically check the validity of integrators that
users develop, within \cpp{grompp}. Such points of validation include:
\begin{itemize}
\item \emph{The Trotter decomposition:} reject patterns which are
  invalid decompositions (v($\Delta t$/2) p ($\Delta t$)
  v($\Delta t$/3), for example),
\item \emph{the element order:} prevent using quantities which
  were not yet calculated for the current timestep,
\item \emph{the integration:} spot dynamical variables whose
  integration does not add up to $\Delta t$,
\item \emph{the communication:} warn if integrators call unnecessary
  communication steps, or if quantities are used which would require
  a global communication previously.
\end{itemize}

\subsection*{More sophisticated integrators}

There are classes of integrators, especially hybrid Monte Carlo
integrators, that relax some of the asssumptions we generally use; for
example, that use variable $\delta t$ or that use a variable number of
steps, or that adjust the kinetic energy distribution, or that
explicitly work with the shadow Hamiltonian.  We will examine the use
of these integrators as well.

\subsection*{I/O Handling}

There may be advantages within the loop of only buffering data within
the integrator, and waiting to perform output until the end of the
loop -- we will investigate the possibilities for such I/O
restructuring.
