\subsection*{Concepts and code flow}

The new code introduces three new main concepts, represented by
classes:
\begin{itemize}
\item \cpp{Element}: An element represents an elementary operation,
  the building blocks of an integrator. Every element has two member
  functions, \cpp{initialize}, which is called once during setup, and
  \cpp{run}, which is the actual action of the element and can be
  called repeatedly during the run of a simulation. Typical examples
  include elements propagating the positions or the velocities,
  elements calculating the forces and/or energies, elements
  enforcing constraints, an element performing global communications,
  and elements saving trajectories or snapshots.
\item \cpp{Integrator}: The integrator is essentially a list of
  \cpp{Element} objects, whose \cpp{run} functions are repeatedly
  executed in order a prescribed number of times. Integrators
  themselves are \cpp{Element} objects, to allow for nested
  integrators (useful, for example, for multistep integrators).
\item \cpp{StateManager}: The state manager holds all data that is
  used throughout the integration. \textbf{This is a temporary
    solution:} it merely exposes the data structures as they were used
  in \cpp{do_md} previously. We propose a fundamental reorganization
  of the data structures, see Section~\ref{sec:data-reorganization}.
  However, the current state of the state manager give some sense of
  the data that is required for the diferent elements to operate.
\end{itemize}

The current code was left untouched wherever possible. The new
integrator formalism is named \emph{custom integrator}. It is invoked
by using the .mdp option \code{integrator = custom}. If this option is
found, a new code path is followed which will be detailed in the next
paragraphs.

In \path{src/programs/mdrun/runner.cpp}, where the type of the
integrator function is chosen depending on the integrator .mdp option,
a new custom integrator case has been added to the enum,
\cpp{eiCUSTOM}. This will direct the code flow into the
\cpp{do_customMD} function, in analogy to the \cpp{eiMD}, \cpp{eiBD},
\cpp{eiSD1}, \cpp{eiVV} enum cases directing the code flow into the
\cpp{do_md} function.  \cpp{do_customMD} is a powerful function, which
will eventually not only be able to reproduce all integrator
algorithms from \cpp{do_md} using the new integrator formalism, but
also provide an extensible code structure to create new integrators
and samplers such as Hybrid Monte Carlo.

The \cpp{do_customMD} function (implemented in
\path{src/programs/mdrun/customintegrator/customMD.cpp}) is hence a
good place to get an overview of the new code flow. Early in the
function, the following objects are created:
\begin{itemize}
\item \emph{Sequence of elements:} A list of pointers to \cpp{Element}
  objects, whose members and order define the integrator algorithm.
\item \emph{Integrator:} The integrator (see above) is defined using
  the previously generated sequence of elements as input.
\item \emph{State manager:} The state manager holds all the necessary
  data for the integrator and the elements to operate (see
  above). Additionally, this object has 4 methods (\cpp{loop_setup},
  \cpp{step_setup}, \cpp{step_teardown}, \cpp{loop_teardown})
  operating on data which does not directly belong to the integrator
  algorithm, but is needed for GROMACS to function (e.g. domain
  decomposition). \emph{Note:} In some places, these functions still
  contain some functionality which is rather integrator-dependent and
  should hence eventually be moved into separate elements. See
  Section~\ref{sec:further-development} for details.
\end{itemize}

The rest of the simulation run is then essentially consisting of eight
steps, three of which are executed repeatedly:
\begin{itemize}
\item \emph{Integrator-independent data initialization:}
  \cpp{state_manager.loopSetup()} initializes data structures which
  are not directly dependent on the integration algorithm.
\item \emph{Element initialization:}
  \cpp{integrator->initialize(state_manager)} initializes the elements
  saved in the different sequences of the integrator. During
  initialization, the elements receive a reference to the
  \cpp{state_manager}, ensuring that elements work on the same data.
\item \emph{Integrator-dependent data initialization:}
  \cpp{integrator->prerun()} runs the elements in the pre-run sequence
  defined before, executing integrator-related tasks that have to be
  done exactly once before the start of the loop (e.g. an initial
  constraining of positions and velocities).
\item \emph{Integrator loop:} The following three actions are repeated
  for every step of the integration.
  \begin{itemize}
  \item \emph{Integrator-independent step initialization:}
    \cpp{state_manager.stepSetup()} does integrator-independent tasks
    needed before every or some steps (e.g. setting variables such as
    \cpp{t} and \cpp{bLastStep}, doing PME load-balancing, etc.).
  \item \emph{Integrator step:} \cpp{integrator->prerun()} loops
    through the elements of the run sequence defined before, calling
    their respective run() methods in order.  This call represents the
    actual integrator.
  \item \emph{Integrator-independent step finalization:}
    \cpp{state_manager.stepSetup()} does integrator-independent tasks
    needed after every or some steps (e.g. resetting counters, doing
    domain decomposition, etc.).
  \end{itemize}
\item \emph{Integrator-dependent data finalization:}
  \cpp{integrator->postrun()} runs the elements in the post-run
  sequence defined earlier, performing integrator-related tasks needed
  during finalization of the simulation.
\item \emph{Integrator-independent data finalization:}
  \cpp{state_manager.loopTeardown()} finalizes the simulation,
  performing tasks such as the closing of TNG file, finalizing
  PME-only nodes, printing final information to log files, etc.
\end{itemize}


\subsection*{Integrator creation}

Currently, the sequences of elements defining the integrator algorithm
are determined by the new \cpp{custom-type} .mdp option.  The
\cpp{SequenceFactory} is creating a specific sequence of elements for
every known \cpp{custom-type}.  This is a temporary solution to test
different algorithms with the new integrator formalism.

In the future, we plan to develop a parser allowing users to give
lists of elements to create new integrator algorithms. For more
details, please see ``Custom integrator parsing'' in
Section~\ref{sec:further-development}.


\subsection*{Connection to current code}

The new integrator formalism code coexists with the standard GROMACS
integrator. Unless the user selects the \cpp{custom} option in the .mdp
file and specifies a \cpp{custom-type}, the GROMACS code works as
usual. All new source files were in a new folder named
\path{customIntegrator} residing in the \path{mdrun} folder. Some
changes to existing source files were unavoidable, the largest one by
far being in \cpp{update.cpp}, where more granular update functions
were added to facilitate the writing of elements.

Many elements call GROMACS functions previously available. As a
notable example, the \cpp{updateForce} element simply calls the
\cpp{do_force} function, where no changes in have been made in the
\cpp{do_force} function. In this way, we ensure to keep the most
performance-critical code paths unchanged.

Keeping the current GROMACS code functional during the initial process of
transition has the additional large advantage that it is always
available as a comparison when re-implementing these algorithms in the
new framework.
