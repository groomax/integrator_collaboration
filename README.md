A place to put documentation for the planned integrator/MC framework.

Currently: 

proposal\_january\_2017: Describes the overall theory.  Will eventually be a supplementary text, possibly a ArXiV paper.

summary\_june\_2017: Accompanying document for the gerrit draft pushed early July.
