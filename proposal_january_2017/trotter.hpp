/*
The short-term proposal to encapsulate different integrator schemes
(into a polymorphic do_one_step() method of different Integrator
implementations) will benefit from discussions related to the roadmaps
for the various state data structures and parallelization schemes,
but the early steps of encapsulation are straightforward and allow
experimentation on the Trotter-based schemes while leaving the current
basic code paths in place.

Long term planning for implementation details of the Elements is
dependent on the data structures and parallelization schemes as well
as optimizatinos deferred until later profiling results. But it would
be helpful to get early feedback on useful patterns or C++11/14 idioms
to
 * allow Integrators with post-build-time or even run-time specified
   element lists as well as compiler-optimized Integrators with
   pre-defined element lists.
 * allow unrolled lists of inlined element functions to avoid
   unnecessary extra passes through the inner loop.
 * allow more sophisticaticated interface to the global communicators
   for elements to request/schedule data for batched communications
 * discuss provisions for parallelization, data flow, or task management
*/

#include <memory>
#include <vector>

//* uncomment to pass a lint-check...
class input_t;

class Element;
class ElementSequence;
class State;

ElementSequence get_elements(input_t& user_input);
void initialize();
void pre_step();
void post_step();
void finalize();

const unsigned int nrepeat(0);
typedef float real;
real dt(0);
const unsigned int first_step(0);
const unsigned int last_step(0);
//*/

class Element
{
private:
    // Element lifetime is ultimately managed by Integrator,
    // which maintains a handle to a system state object that provides
    // accessor methods to manage specific structures.
    const State& state_;
public:
    Element(const State& state);
    virtual void do_update() const;
};

class ElementSequence
{
private:
    std::vector< std::shared_ptr<Element> > elements_;
};

class Integrator
{
private:
    const ElementSequence elements_;
    const unsigned int nrepeat_;
public:
    Integrator(State& state,
                const ElementSequence& elements,
                unsigned int nrepeat,
                real dt);
    virtual ~Integrator();

    void do_one_step();
};

class PositionUpdate : public Element
{
private:
public:
    // One option is to implement do_one_step by calling a polymorphic method
    // to get each element's updates to the state data structures
    virtual void do_update() const;

    // Another option is to implement do_one_step by calling a sequence
    // of registered functions, providing a more straightforward refactoring
    // from a short term encapsulation of integrator functionality to the more
    // flexible long-term plan.
    void operator()(State& state) const;
};

Integrator build_integrator(input_t& user_input, State& state);
/*{
    // Hypothetical element container built from user input.
    const ElementSequence& elements = get_elements(user_input);
    Integrator integrator(state, elements, nrepeat, dt);
    return integrator;
}*/

void calling_code(input_t& input, State& state)
{
    // Let Integrator take over lifetime of Elements
    Integrator integrator = build_integrator(input, state);
    initialize();
    for (unsigned int i = first_step; i <= last_step; ++i)
    {
        pre_step();
        integrator.do_one_step();
        post_step();
    }
    finalize();
}

/* Alternative implementations
   * Only the update method of the Element is registered with the integrator
   * Inline-able do_update Integrator methods are templated on Element type
   * Integrator and/or ElementSequence is templated, with specializations
   to facilitate precompiled (and compiler-optimized) standard integrators

It seems like there are a number of ways to implement the flexibility
to allow both run-time specified element sequences as well as
pre-defined compiler-optimized element sequences. As an Alternative
to the fused-element approach to optimizing out the repeated
inner loop, I suspect there is some combination of templates and/or
lambdas that would allow elements to be implemented with inline-able
functions to bundle inner loop iterations, letting the compiler
do the fusing.

E.g.
foreach element_method_list in irreducible_dataflow_block
{
    foreach atom in atoms
    {
        unrollable_foreach process_particle in element_method_list
        {
            process_particle(atom);
        }
    }
}
*/
